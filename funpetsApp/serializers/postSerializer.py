from funpetsApp.models.post import Post
from rest_framework import serializers

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = [
            'post_id', 
            'post_creation_date', 
            'post_text', 
            'post_image_ref', 
            'post_video_ref', 
            'post_gif_ref', 
            'post_account_id', 
            'post_typepost_id', 
            'post_parent_id'
            ]