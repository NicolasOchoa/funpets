from funpetsApp.models.reaction import Reaction
from rest_framework import serializers
class ReactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reaction
        #fields = '__all__'
        fields=[
            'reaction_id',
            'reaction_creation_date,',
            'reaction_post_id',
            'reaction_user_id',
            'reaction_typereaction_id'
            ]