from rest_framework import generics, status

from funpetsApp.models.account import Account
from funpetsApp.serializers.accountSerializer import AccountSerializer

from django.http import JsonResponse

class UserAccountListView(generics.RetrieveAPIView):
    
    def get(self, request, *args, **kwargs):

        queryset = Account.objects.filter(account_user_id = kwargs['upk'])
        serializer = AccountSerializer(queryset, many=True)

        return JsonResponse(serializer.data, safe=False)
        