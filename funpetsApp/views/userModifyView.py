from django.shortcuts import render
from funpetsApp.models.user import User
from funpetsApp.serializers.userSerializer import UserSerializer
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import generics, status

class UserModifyView(generics.UpdateAPIView):

    def patch(request, *args, **kwargs):
        User_data = JSONParser().parse(request)
        User_Serializer = UserSerializer(User, data=User_data)
        if request.method == 'PUT':
            if User_Serializer.is_valid():
                User_Serializer.save()
            return JsonResponse(User_Serializer.data)
        return JsonResponse(User_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)