from rest_framework import generics, status

from funpetsApp.models.post import Post
from funpetsApp.serializers.postSerializer import PostSerializer

from django.http import JsonResponse

class PostsListView(generics.RetrieveAPIView):
    
    def get(self, request, *args, **kwargs):

        queryset = Post.objects.all().order_by('-post_id')
        serializer = PostSerializer(queryset, many=True)

        return JsonResponse(serializer.data, safe=False)
        