from django.db import models
from .user import User
from .account import Account

class Follower(models.Model):
    follower_id = models.AutoField(primary_key=True)
    follower_creation_date =models.DateTimeField(auto_now_add=True, blank=True)

    follower_account_id = models.ForeignKey(Account, on_delete = models.CASCADE)
    follower_user_id = models.ForeignKey(User, on_delete = models.CASCADE)