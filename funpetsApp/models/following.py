from django.db import models
from .user import User
from .account import Account

class Following(models.Model):
    following_id=models.AutoField(primary_key=True)
    following_creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    
    following_user_id = models.ForeignKey(User, on_delete = models.CASCADE)
    following_account_id = models.ForeignKey(Account, on_delete = models.CASCADE)
