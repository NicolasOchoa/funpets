from django.db import models

from .account import Account
from .typepost import TypePost

class Post(models.Model):
    post_id = models.AutoField(primary_key=True)
    post_creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    post_text = models.CharField('Text', null=True, max_length = 256)
    post_image_ref = models.CharField('Image_ref', null=True, max_length = 256)
    post_video_ref = models.CharField('Video_ref', null=True, max_length = 256)
    post_gif_ref = models.CharField('Gif_ref', null=True, max_length = 256)
   
    post_account_id = models.ForeignKey(Account, on_delete=models.CASCADE)
    post_typepost_id = models.ForeignKey(TypePost, on_delete=models.CASCADE)
    post_parent_id = models.IntegerField('Parent_id', null=True)