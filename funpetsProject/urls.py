"""funpetsProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from funpetsApp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    #vistas logicas 
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('userAccounts/<int:upk>/', views.UserAccountListView.as_view()),
    path('account/<int:upk>/', views.AccountCreateView.as_view()),
    path('account/<int:upk>/<int:pk>/', views.AccountDetailView.as_view()),   
    path('post/<int:upk>/', views.PostCreateView.as_view()),
    path('post/<int:upk>/<int:pk>/', views.PostDetailView.as_view()),
    path('posts/', views.PostsListView.as_view()),
    path('reaction/<int:upk>/', views.ReactionCreateView.as_view()),
    path('reaction/<int:upk>/<int:pk>/', views.ReactionDetailView.as_view()),
    path('follower/<int:upk>/', views.FollowerCreateView.as_view()),
    path('follower/<int:upk>/<int:pk>/', views.FollowerDetailView.as_view()),
    path('following/<int:upk>/', views.FollowingCreateView.as_view()),
    path('following/<int:upk>/<int:pk>/', views.FollowingDetailView.as_view()),
    path('typepet/', views.TypePetCreateView.as_view()),
    path('typepet/<int:pk>/', views.TypePetDetailView.as_view()),
    path('typereaction/', views.TypeReactionCreateView.as_view()),
    path('typepost/', views.TypePostCreateView.as_view()),
    path('typepost/<int:pk>/', views.TypePostDetailView.as_view()),

]
